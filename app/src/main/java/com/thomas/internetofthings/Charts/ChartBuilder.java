package com.thomas.internetofthings.Charts;

import android.content.Context;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.AxisValueFormatter;
import com.thomas.internetofthings.Charts.ThinkSpeak.Channel;
import com.thomas.internetofthings.Charts.ThinkSpeak.Feed;

import org.joda.time.Duration;
import org.joda.time.Instant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomot on 8/08/2016.
 * Builder to create the LineCharts. Can be modified with an Axis formatter etc.
 */
public class ChartBuilder {

    private final List<LineDataSet> mLineDataSets = new ArrayList<>();
//    private final List<String> mXLabels = new ArrayList<>();
    private final Instant mStartTime;
    private final Context mContext;

    public List<String> feedNames;

    /**
     * This constructor converts the objects into a way to return a chart.
     * First converts the objects to be stored
     *
     * @param channel represents the abstraction of the ThinkSpeak Channel
     * @param feeds   a list of feed objects
     */
    public ChartBuilder(Channel channel, List<Feed> feeds, Context context) {

        // Go through the list of fields and convert each into entries
        // ArrayLists are initialised first
        // A list of lists is created and for each field a new entry is created.
        int fieldCount = channel.getFieldCount();
        int feedCount = feeds.size();
        List<List<Entry>> entries = new ArrayList<>(fieldCount);
        mStartTime = feeds.get(0).createdAt.toInstant();

        for (int i = 0; i < fieldCount; i++) {
            entries.add(new ArrayList<Entry>(feedCount));
        }

        // Fill arrays with correct values
        // Saves the start time for X Axis value calculation
        // Dates are normalised and then added as minute counts
        for (Feed feed : feeds) {
            Duration duration = new Duration(mStartTime, feed.createdAt);
            for (int i = 0; i < fieldCount; i++) {
                List<Entry> list = entries.get(i);
                list.add(new Entry((int) duration.getStandardSeconds(), feed.fields.get(i)));
            }
        }

        // Fill up the internal LineDataSets
        for (int d = 0; d < fieldCount; d++) {
            mLineDataSets.add(new LineDataSet(entries.get(d), channel.fieldNames.get(d)));
        }

        feedNames = channel.fieldNames;
        mContext= context;
    }

    public void buildChart(LineChart lineChart, int chartNumber) {

        LineData lineData = new LineData();
        lineData.addDataSet(mLineDataSets.get(chartNumber));

        AxisValueFormatter formatter = new AxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return mStartTime.plus((int) value * 1000).toDateTime().toString("hh:mm a");
            }

            @Override
            public int getDecimalDigits() {
                return 0;
            }
        };

//        LineChart lineChart = new LineChart(mContext);
        lineChart.setData(lineData);
        lineChart.setHighlightPerTapEnabled(false);
        lineChart.setHighlightPerDragEnabled(false);
        lineChart.setDescription("");
        lineChart.setNoDataText("Error no data to display");

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter(formatter);

        lineChart.getAxisRight().setEnabled(false);

        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setDrawZeroLine(true);
        yAxis.setDrawGridLines(false);

//        return lineChart;
    }

}