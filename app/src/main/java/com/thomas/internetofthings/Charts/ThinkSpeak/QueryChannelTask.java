package com.thomas.internetofthings.Charts.ThinkSpeak;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.thomas.internetofthings.GraphActivity;
import com.thomas.internetofthings.R;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by tomot on 13/08/2016.
 * This class returns an input stream of the ThinkSpeak Channel
 */
public class QueryChannelTask extends AsyncTask<Integer, Void, Channel>{

    private ListView mListView;
    private Context mContext;
    private ArrayList<Feed> mFeedList;

    public QueryChannelTask(Context mContext, ListView mListView) {
        this.mContext = mContext;
        this.mListView = mListView;
    }

    @Override
    protected Channel doInBackground(Integer... integers)  {

        // Assume that there is only one int
        Integer channelNumber = integers[0];
        HttpURLConnection urlConnection = null;

        String urlString = "https://api.thingspeak.com/channels/" + channelNumber + "/feeds.json";
        try {
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            BufferedInputStream inputStream= new BufferedInputStream(urlConnection.getInputStream());
            JsonDataReader jsonReader = new JsonDataReader(inputStream);
            jsonReader.readJson();

            mFeedList = jsonReader.mFeedList;
            return jsonReader.mChannel;

        } catch (IOException e) {
            Toast toast = Toast.makeText(mContext, "Error in JSON", Toast.LENGTH_LONG);
            toast.show();
            return null;
        }
//        finally {
//            if (urlConnection != null) {
//                urlConnection.disconnect();
//            }
//        }
    }

    @Override
    protected void onPostExecute(final Channel channel) {
        super.onPostExecute(channel);

        if (channel != null) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, R.layout.list_graph, R.id.graph_name, channel.fieldNames);
            mListView.setAdapter(adapter);

            mListView.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Intent intent = new Intent(mContext, GraphActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("channel", channel);
                            intent.putParcelableArrayListExtra("feed", mFeedList);
                            intent.putExtra("number", i);
                            mContext.startActivity(intent);
                        }
                    }
            );
        }
    }
}

