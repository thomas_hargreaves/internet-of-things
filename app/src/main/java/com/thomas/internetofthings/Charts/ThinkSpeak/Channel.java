package com.thomas.internetofthings.Charts.ThinkSpeak;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomot on 8/08/2016.
 * The Channel Class represents the Java implementation of the
 */
public class Channel implements Parcelable{

    private final int mId;
    public final String title;
    public final String description;
    private final double mLatitude;
    private final double mLongitude;
    private final DateTime mCreatedTime;
    private final DateTime mUpdatedTime;
    private final int mLastEntryId;
    public final List<String> fieldNames;

    Channel (int id, String title, String description, double latitude, double longitude, DateTime createdTime, DateTime updatedTime, int lastEntryId, List<String> fieldNames) {
        mId = id;
        this.title = title;
        this.description = description;
        mLatitude = latitude;
        mLongitude = longitude;
        mCreatedTime = createdTime;
        mUpdatedTime = updatedTime;
        mLastEntryId = lastEntryId;
        this.fieldNames = fieldNames;
    }

    public int getFieldCount() {
        return fieldNames.size();
    }

    protected Channel(Parcel in) {
        mId = in.readInt();
        title = in.readString();
        description = in.readString();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mCreatedTime = (DateTime) in.readValue(DateTime.class.getClassLoader());
        mUpdatedTime = (DateTime) in.readValue(DateTime.class.getClassLoader());
        mLastEntryId = in.readInt();
        if (in.readByte() == 0x01) {
            fieldNames = new ArrayList<>();
            in.readList(fieldNames, String.class.getClassLoader());
        } else {
            fieldNames = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
        dest.writeValue(mCreatedTime);
        dest.writeValue(mUpdatedTime);
        dest.writeInt(mLastEntryId);
        if (fieldNames == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(fieldNames);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Channel> CREATOR = new Parcelable.Creator<Channel>() {
        @Override
        public Channel createFromParcel(Parcel in) {
            return new Channel(in);
        }

        @Override
        public Channel[] newArray(int size) {
            return new Channel[size];
        }
    };

}
