package com.thomas.internetofthings.Charts.ThinkSpeak;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomot on 9/08/2016.
 * This class Feed
 */
public class Feed implements Parcelable{

    int mId;
    int mFieldCount;
    public DateTime createdAt;
    public List<Float> fields;

    /**
     * A single instance of the Feed Class that represents a piece of data from ThinkSpeak
     * @param id A given value
     * @param createdAt Converted from the date string
     * @param fieldValues An array containing all the values of the fields (max eight)
     */
    Feed(int id, DateTime createdAt, List<Float> fieldValues) {
        mId = id;
        this.createdAt = createdAt;
        fields = fieldValues;

        mFieldCount = fieldValues.size();
    }

    protected Feed(Parcel in) {
        mId = in.readInt();
        mFieldCount = in.readInt();
        createdAt = (DateTime) in.readValue(DateTime.class.getClassLoader());
        if (in.readByte() == 0x01) {
            fields = new ArrayList<Float>();
            in.readList(fields, Float.class.getClassLoader());
        } else {
            fields = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeInt(mFieldCount);
        dest.writeValue(createdAt);
        if (fields == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(fields);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Feed> CREATOR = new Parcelable.Creator<Feed>() {
        @Override
        public Feed createFromParcel(Parcel in) {
            return new Feed(in);
        }

        @Override
        public Feed[] newArray(int size) {
            return new Feed[size];
        }
    };

}
