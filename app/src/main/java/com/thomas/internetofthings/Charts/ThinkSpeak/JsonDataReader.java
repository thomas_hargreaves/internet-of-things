package com.thomas.internetofthings.Charts.ThinkSpeak;

import android.util.JsonReader;

import org.joda.time.DateTime;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomot on 8/08/2016.
 * Class that reads data from multiple sources and returns a new instance of Channel object
 *
 */
public class JsonDataReader {

    private JsonReader mReader;

    public Channel mChannel;
    public ArrayList<Feed> mFeedList = new ArrayList<>();

    public JsonDataReader(InputStream Json) {
        mReader = new JsonReader(new InputStreamReader(Json));
        mReader.setLenient(true);
    }

    public void readJson() throws IOException{

        try {
            parse();
        } finally {
            mReader.close();
        }
    }

    private void parse() throws IOException{

        mReader.beginObject();
        while (mReader.hasNext()) {
            String name = mReader.nextName();
            switch (name) {
                case "channel":
                    readChannel();
                    break;
                case "feeds":
                    readFeeds();
                    break;
            }
        }
        mReader.endObject();

    }

    // Processes the reading of the 'feed' object in the ThinkSpeak return string
    private void readFeeds() throws IOException{

        mReader.beginArray();
        while (mReader.hasNext()) {
            readSingleFeed();
        }
        mReader.endArray();
    }

    private void readSingleFeed() throws IOException{

        DateTime date = null;
        List<Float> fields = new ArrayList<>(8);
        int id = 0;

        mReader.beginObject();
        while (mReader.hasNext()) {

            String name = mReader.nextName();

            if (name.equals("created_at")) {
                // date = sDateFormat.parse(mReader.nextString());
                date = new DateTime(mReader.nextString());
            } else if (name.equals("entry_id")) {
                id = mReader.nextInt();
            } else if (name.startsWith("field")) {
                fields.add((float) mReader.nextDouble());
            }
        }
        mReader.endObject();

        // Checks and adds a new feed to the list.
        if (id > 0 && date != null){
            mFeedList.add(new Feed(id, date, fields));
        }
    }

    // Creates a Channel object and saves to the mChannel Field
    private void readChannel() throws IOException{

        int id = 0;
        int finalId = 0;
        double latitude = 0;
        double longitude = 0;
        String channelName = null;
        String description = null;
        DateTime createdAt = null;
        DateTime updatedAt = null;
        List<String> fieldNames = new ArrayList<>(8);

        mReader.beginObject();
        while(mReader.hasNext()) {

            String name = mReader.nextName();

            if (name.equals("id")) {
                id = mReader.nextInt();
            } else if (name.equals("name")) {
                channelName = mReader.nextString();
            } else if (name.equals("description")) {
                description = mReader.nextString();
            } else if (name.equals("latitude")) {
                latitude = mReader.nextDouble();
            } else if (name.equals("longitude")) {
                longitude = mReader.nextDouble();
            } else if (name.startsWith("field")) {
                fieldNames.add(mReader.nextString());
            } else if (name.equals("created_at")) {
                createdAt = new DateTime(mReader.nextString());
            } else if (name.equals("updated_at")) {
                updatedAt = new DateTime(mReader.nextString());
            } else if (name.equals("last_entry_id")) {
                finalId = mReader.nextInt();
            }
        }
        mReader.endObject();

        // Save as the channel object in the class
        if (id > 0 && channelName != null && description != null && createdAt != null && updatedAt !=null && finalId > 0)
        {
            mChannel = new Channel(id, channelName, description, latitude, longitude, createdAt, updatedAt, finalId, fieldNames);
        }
    }

}


