package com.thomas.internetofthings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.github.mikephil.charting.utils.Utils;
import com.thomas.internetofthings.Charts.ThinkSpeak.QueryChannelTask;

public class MainActivity extends AppCompatActivity {

    static final int GET_CHANNEL = 1;
    private int mChannel = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // TODO save instance state.

        // Initialise MP Chart
        Utils.init(getApplicationContext());

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.main_title);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            Intent i = new Intent(this, SetupActivity.class);
            startActivityForResult(i, GET_CHANNEL);
        } else {
            int channelNumber = savedInstanceState.getInt("channel");
            if (channelNumber == -1) {
                Intent i = new Intent(this, SetupActivity.class);
                startActivityForResult(i, GET_CHANNEL);
            }
        }

        // Setup the button
        Button button = (Button) findViewById(R.id.change_channel);
        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), SetupActivity.class);
                        startActivityForResult(intent, GET_CHANNEL);
                    }
                }
        );
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("channel", mChannel);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_CHANNEL) {
            if (resultCode == RESULT_OK) {
                mChannel = data.getIntExtra("channel", -1);
                ListView listView = (ListView) findViewById(R.id.graph_list);
                new QueryChannelTask(getApplicationContext(), listView).execute(mChannel);
            }
        }
    }
}
