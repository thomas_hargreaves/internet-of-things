package com.thomas.internetofthings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SetupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Setup");

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        setSupportActionBar(toolbar);

        Button button = (Button) findViewById(R.id.fetch_channel);
        button.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        EditText temp = (EditText) findViewById(R.id.channel_number);
                        String channelString = temp.getText().toString();
                        int channelNumber;

                        try {
                            channelNumber = Integer.parseInt(channelString);
                            Intent data = new Intent();
                            data.putExtra("channel", channelNumber);
                            setResult(RESULT_OK, data);
                            finish();
                        } catch (NumberFormatException e) {
                            // Display message
                            Toast toast = Toast.makeText(getApplicationContext(), "Invalid number", Toast.LENGTH_SHORT);
                            toast.show();
                        }

                    }
                }
        );

        // Testing method :)
        Button test_button = (Button) findViewById(R.id.test_button);
        if (test_button != null) {
            test_button.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                int channelNumber = 133401;
                                Intent data = new Intent();
                                data.putExtra("channel", channelNumber);
                                setResult(RESULT_OK, data);
                                finish();
                            } catch (NumberFormatException e) {
                                Toast toast = Toast.makeText(getApplicationContext(), "Invalid number", Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        }
                    }
            );
        }

    }
}
