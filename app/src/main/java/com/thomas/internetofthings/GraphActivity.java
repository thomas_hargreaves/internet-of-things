package com.thomas.internetofthings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.thomas.internetofthings.Charts.ChartBuilder;
import com.thomas.internetofthings.Charts.ThinkSpeak.Channel;
import com.thomas.internetofthings.Charts.ThinkSpeak.Feed;

import java.util.ArrayList;

public class GraphActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);



        Intent intent = getIntent();
        if (intent != null) {
            Channel channel = intent.getParcelableExtra("channel");
            ArrayList<Feed> feeds = intent.getParcelableArrayListExtra("feed");
            int chartNumber = intent.getIntExtra("number", -1);

            if (channel != null && feeds != null && chartNumber >= 0) {
                LineChart lineChart = (LineChart) findViewById(R.id.graph);
                ChartBuilder chartBuilder = new ChartBuilder(channel, feeds, getApplicationContext());
                chartBuilder.buildChart(lineChart, chartNumber);
                lineChart.invalidate();

                // Set Title, Axis and Description
                TextView yAxisLabel = (TextView) findViewById(R.id.y_axis);
                yAxisLabel.setText(channel.fieldNames.get(chartNumber));

                TextView description = (TextView) findViewById(R.id.description);
                description.setText(channel.description);

                toolbar.setTitle("Graph of " + channel.title);
            }
        }

        setSupportActionBar(toolbar);

    }

}
